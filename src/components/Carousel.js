
import React, { Component } from "react";
import Slider from "react-slick";
import image1 from '../assets/arts-del-valle-9149631-no-1.jpg'
import image2 from '../assets/arts-del-valle-9149631-no-2.jpg'
import image3 from '../assets/arts-del-valle-9149631-no-3.jpg'
import image4 from '../assets/arts-del-valle-9149631-no-4.jpg'
import image5 from '../assets/ph01-arts-del-valle-3023-9149631-no-1.jpg'
import image6 from '../assets/puerta-del-centro-9149374-no-1.jpg'
import image7 from '../assets/tipo-1-arts-del-valle-3015-9149631-no-1.jpg'
import image8 from '../assets/tipo-1-arts-del-valle-3015-9149631-no-2.jpg'
import image9 from '../assets/tipo-2-arts-del-valle-3019-9149631-no-1.jpg'


class Carousel extends Component {
  render() {
    const settings = {
      dots: true,
      infinite: true,
      speed: 500,
      slidesToShow: 1,
      slidesToScroll: 1
    };
    return (
      <div>
        <h2> Single Item</h2>
        <Slider {...settings}>
          <div>
            <img src={image1} alt="edificio1"/>
          </div>
          <div>
            <img src={image2} alt="edificio2"/>
          </div>
          <div>
            <img src={image3} alt="edificio3"/>
          </div>
          <div>
            <img src={image4} alt="edificio4"/>
          </div>
          <div>
            <img src={image5} alt="edificio5"/>
          </div>
          <div>
            <img src={image6} alt="edificio6"/>
          </div>
          <div>
            <img src={image7} alt="edificio7"/>
          </div>
          <div>
            <img src={image8} alt="edificio8"/>
          </div>
          <div>
            <img src={image9} alt="edificio9"/>
          </div>
        </Slider>
      </div>
    );
  }
}

export default Carousel