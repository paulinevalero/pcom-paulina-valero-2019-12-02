import React, {Component} from 'react'
import data from '../db/data.json'
import CardPropiedad from './CardPropiedad.js'

class Home extends Component {

  render(){
    
    return(
      <div>
        <div className="container">
          {data.map(({property}) => (
            <CardPropiedad key={property.id} property={property}/>
          ))}            
        </div>
      </div>
    )
  }
}

export default Home 