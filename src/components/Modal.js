import React from 'react'
import img from '../assets/puerta-del-centro-9149374-no-1.jpg'
import Carousel from './Carousel'

const Modal = ({isOpen, closeGallery}) => {
  return (
    <div>
      <div className={isOpen?'modal is-active' : 'modal'}>
        <div onClick={closeGallery} className="modal-background"></div>
        <div className="modal-content">
          <p className="image is-full">
            <Carousel/>
          </p>
        </div>
        <button onClick={closeGallery} className="modal-close is-large" aria-label="close"></button>
      </div>
    </div>
  )
}

export default Modal
