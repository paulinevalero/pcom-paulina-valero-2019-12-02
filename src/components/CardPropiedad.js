import React, {Component} from 'react'
import img from '../assets/puerta-del-centro-9149374-no-1.jpg'
import Modal from './Modal'


class CardPropiedad extends Component{
  state = {
    isOpen: false 
  }
  displayGallery = () => {
    this.setState({isOpen: true})
  }
  closeGallery = () => {
    this.setState({isOpen: false})
  }

  render() {
    const {property} = this.props
    const {name, price, description, type_operation, type_property} = property
    const {street, number_ext, number_int, city} = property.address
    const {isOpen} = this.state 
    return (
      <div>
        <div className="card is-horizontal columns is-gapless">
          <div className="column is-3">
            <figure className="image is-4by3">
              <img src={img} alt={name} />
            </figure>
          </div>
          <div className="card-content">
            <div className="media">
              <div className="media-content">
                <p className="title is-4">{`${street} #${number_ext} Int. ${number_int}`}</p>
                <p>{`$ ${price} MDP`}</p>
                <p className="subtitle is-6"><span className="upper">{type_operation} </span>
                de
                <span className="upper"> {type_property} </span>
                {`en ${city}`}
                </p>
              </div>
            </div>

            <div className="content">
              <p>{description}</p> 
              <button onClick={this.displayGallery} className="button is-success is-outlined">Ver propiedad</button>
              <Modal isOpen={isOpen} closeGallery={this.closeGallery}/> 
            </div>
          </div>  
        </div>
        
      </div>
    )
  }
}


export default CardPropiedad
